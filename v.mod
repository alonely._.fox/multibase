Module {
	name: 'multibase'
	description: 'Multiple base encoding/decoding in V'
	version: '0.0.1'
	license: 'MIT'
	repo_url: 'https://github.com/islonely/multibase'
	author: 'Adam Oates'
	dependencies: ['base58', 'base36', 'base32', 'hex']
}
